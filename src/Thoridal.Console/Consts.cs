﻿namespace Thoridal.Console
{
    internal static class Consts
    {
        public static class IdentityServer
        {
            public const string Url = "TBD";

            public static class Api
            {
                public const string Url = "TBD";
                public const string Id = "TBD";
            }

            public static class Client
            {
                public const string Id = "TBD";
                public const string RedirectUri = "http://127.0.0.1";
                public static readonly string Scope = $"openid profile email offline_access {Api.Id} ";
            }
        }
    }
}
