﻿namespace Thoridal.Console.Sdk
{
    using System.Reflection;
    using McMaster.Extensions.CommandLineUtils;

    internal static class CommandLineApplicationExtensions
    {
        public static void ShowVersionAndHelp(this CommandLineApplication app)
        {
            app.Out.WriteLine($"Payment Console [{GetInformationalVersion(typeof(Program).Assembly)}]");
            app.Out.WriteLine();

            app.ShowHelp();
        }

        private static string GetInformationalVersion(Assembly assembly)
        {
            var attribute = assembly.GetCustomAttribute<AssemblyInformationalVersionAttribute>();

            var versionAttribute = attribute == null
                ? assembly.GetName().Version.ToString()
                : attribute.InformationalVersion;

            return versionAttribute;
        }
    }
}
