﻿namespace Thoridal.Console.Persistence
{
    public interface ICommandDataRepository
    {
        CommandData GetCommandData();

        void SetCommandData(CommandData commandData);
    }
}
