﻿using McMaster.Extensions.CommandLineUtils;
using System.Threading.Tasks;

namespace Thoridal.Console.Commands.Statement
{
    internal class StatementCommand : ICommand
    {
        private StatementCommand()
        {
        }

        public static void Configure(CommandLineApplication app, CommandLineOptions options, IConsole console)
        {
            // description
            app.Description = "Download a statement";

            // arguments

            // options
            var optionFormat = app.Option("-f|--format", "json or csv", CommandOptionType.SingleOrNoValue);
            app.HelpOption();

            // action (for this command)
            app.OnExecute(
                () =>
                {
                    System.Console.WriteLine("Hello");
                });
        }

        public async Task ExecuteAsync(CommandContext context)
        {
            System.Console.WriteLine("OK");
        }

        public class Reset : ICommand
        {
            public Task ExecuteAsync(CommandContext context)
            {
                context.Repository.SetCommandData(null);
                return Task.CompletedTask;
            }
        }
    }
}
