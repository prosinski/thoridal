﻿namespace Thoridal.Console.Commands
{
    using McMaster.Extensions.CommandLineUtils;
    using Thoridal.Console.Persistence;

    public class CommandContext
    {
        public CommandContext(
            IConsole console,
            IReporter reporter,
            ICommandDataRepository repository)
        {
            this.Console = console;
            this.Reporter = reporter;
            this.Repository = repository;
        }

        public IConsole Console { get; }

        public IReporter Reporter { get; }

        public ICommandDataRepository Repository { get; }
    }
}
