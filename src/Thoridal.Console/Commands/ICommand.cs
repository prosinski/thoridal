﻿namespace Thoridal.Console.Commands
{
    using System.Threading.Tasks;

    public interface ICommand
    {
        Task ExecuteAsync(CommandContext context);
    }
}
